#pragma once

#include "State.h"
#include "io.h"

#include <iostream>
#include <set>
#include <queue>
#include <unordered_set>


template <class State_t>
class Heuristics
{
public:


	static size_t GetManhattenDistance(const State_t& state)
	{

		static auto diff = [](size_t lhs, size_t rhs)
		{
			return (lhs > rhs) ? lhs - rhs : rhs - lhs;
		};

		static auto getManhattenDistanceForTiles = [](size_t goal, size_t actual)
		{
			//diferena dintre linii
			return diff(goal / State_t::Dimension, actual / State_t::Dimension) +
				//diferenta dintre coloane
				diff(goal % State_t::Dimension, actual % State_t::Dimension);
		};

		size_t distance = 0;
		size_t index = 0;
		for (auto it = state.GetData().begin(); it != state.GetData().end(); ++it, ++index)
		{
			if (*it != 0)
			{
				distance += getManhattenDistanceForTiles(index, *it - 1);
			}
		}

		return distance;
	}

	static size_t GetScore(const State_t& state, const Moves& moves)
	{
		return Heuristics::GetManhattenDistance(state) + moves.size() + 2 * GetNumberOfLinearConflicts(state);
	}

	static size_t GetNumberOfLinearConflicts(const State_t& state)
	{

		auto hasLinearConflict = [](auto begin, auto end, auto check)
		{
			auto it = std::find_if(begin, end, check);

			while (it != end)
			{
				auto next = std::find_if(std::next(it), end, check);

				for (auto otherIt = next; otherIt != end; otherIt = std::find_if(std::next(otherIt), end, check))
				{
					if (*it > *otherIt)
						return true;
				}
				it = next;
			}

			return false;
		};

		size_t numberOfConflicts = 0u;
		for (size_t row = 0u; row < State_t::Dimension; ++row)
		{
			if (hasLinearConflict(state.GetRowIteratorBegin(row), state.GetRowIteratorEnd(row), [row](State_t::ElementType tile) ->bool
				{ return (tile != 0 && (tile - 1) / State_t::Dimension == row); }))
			{
				++numberOfConflicts;
			}
		}

		for (size_t column = 0u; column < State_t::Dimension; ++column)
		{
			if (hasLinearConflict(state.GetColumnIteratorBegin(column), state.GetColumnIteratorEnd(column), [column](State_t::ElementType tile) -> bool
				{ return (tile != 0 && (tile - 1) % State_t::Dimension == column ); }))
			{
				++numberOfConflicts;
			}
		}

		return numberOfConflicts;
	}
};


class Solver
{
public:

	template <class State_t>
	static size_t ComputeHash(const State_t& state)
	{
		static const std::hash<State_t::ElementType> hasher;

		size_t seed = 0u;
		for (auto&& value : state.GetData())
		{
			seed ^= hasher(value) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
		}

		return seed;
	}

	template<class State_t>
	static Moves SolveBFS(const State_t& initialState)
	{
		std::cout << "Solving..." << std::endl << initialState;
		Validate(initialState);
		if (initialState.IsGoalState()) return {}; // no moves required. Initial state is goal state.

		using Node = std::pair<State_t, Moves>;

		auto priorityComapre = [](const Node& lhs, const Node& rhs) -> bool
		{
			return Heuristics<State_t>::GetScore(lhs.first, lhs.second) > Heuristics<State_t>::GetScore(rhs.first, rhs.second);
		};

		//std::queue<Node> openSet;
		std::priority_queue<Node, std::vector<Node>, decltype(priorityComapre) > openSet(priorityComapre);
		openSet.push({ initialState,{} });

		// Create a comparator so std::set can work with the State instances.
		// It doesn't really make sense to compare states otherwise...
		auto stateEquals = [](const State_t& first, const State_t& second)
		{
			return first.GetData() == second.GetData();
		};

		std::unordered_set<State_t, std::function<size_t(const State_t&)>, decltype(stateEquals) >
			closedSet(8u, &Solver::ComputeHash<State_t>, stateEquals);

		while (!openSet.empty())
		{
			auto currentNode    = openSet.top();
			auto&& currentState = currentNode.first;
			auto&& currentMoves = currentNode.second;
			openSet.pop();

			// some logging
			static size_t maxDepth = 0;
			if (currentMoves.size() > maxDepth)
			{
				maxDepth = currentMoves.size();
				std::cout << "Max Depth: " << maxDepth << std::endl;
			}
			// end logging

			if (currentState.IsGoalState())
			{
				std::cout << "Visited: " << closedSet.size() << std::endl;
				return currentMoves;
			}

			closedSet.insert(currentState);

			for (auto&& childMovePair : currentState.GetChildren())
			{
				auto&& childState = childMovePair.first;
				MoveDirection move = childMovePair.second;

				if (closedSet.find(childState) == closedSet.end())
				{
					Moves childMoves = currentMoves;
					childMoves.push_back(move);
					openSet.push({ std::move(childState), std::move(childMoves) });
				}
			}
		}

		throw std::runtime_error("Couldn't solve");
	}

private:

	template<class State_t>
	static void Validate(const State_t& state)
	{
		if (!state.IsValid())
		{
			throw std::runtime_error("Initial state is invalid!");
		}

		if (!state.IsSolvable())
		{
			throw std::runtime_error("Initial state is not solvable!");
		}
	}
};